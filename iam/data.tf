locals {
  iam_policy_name = "${var.name_prefix}-${terraform.workspace}-iam_policy"
  iam_role_name = "${var.name_prefix}-${terraform.workspace}-iam_role"
  aws_region= "ap-southeast-1"
  account_id = "514741091671"
} 
data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
# data "aws_iam_policy_document" "example" {
#   statement {
#     sid = "1"

#     actions = [
#       "s3:ListAllMyBuckets",
#       "s3:GetBucketLocation",
#     ]

#     resources = [
#       "arn:aws:s3:::*",
#     ]
#   }

#   statement {
#     actions = [
#       "s3:ListBucket",
#     ]

#     resources = [
#       "arn:aws:s3:::skundu-proj3-3p-installers/download/*",
#     ]
#   }
# } 
data "aws_iam_policy_document" "example" {
  statement {
    sid = "1"

    actions = [
      "s3:Put*",
      "s3:Get*",
    ]

    resources = [
      "arn:aws:s3:::terraform-tfstate-demo1/*",
      "arn:aws:s3:::imran-cross-account-access-poc/*",
      "arn:aws:s3:::308737788450-cross-account-access/*"
    ]
  }

  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::308737788450:role/access-to-imran-account-role",
    ]
  }
} 